# CA Web App
---
### Running the App
---
**Build and Host**

To run, go to the root folder `cawebapp/`, run `npm install` or `npm i` and then run `NODE_ENV=[dev/qa/beta/prod] node server`
If you don't provide the `NODE_ENV` argument, `dev` will be selected by default.

Once the command runs, the last 2 lines of console output will be:
```
...
Environment: [dev/qa/beta/prod]
Server started on port: 4002
```
and you can go to [localhost:4002](http://localhost:4002) to run the app on browser.

**Note**: While running `npm install`, if you get a selection of installation for semantic-ui in command prompt, please select skip the installation entirely as `semantic.json` file is already present and everything has been configured already.

**Development**

You can run `npm run [dev/qa/beta/prod]` from root folder while development to watch live changes.

---
#### About Code
---
The entry point of app is `cawebapp/src/index.js`. All the global styles are imported here.

**FOLDER STRUCTURE:**
---
Folder | Purpose
---|---
`src/components` | All the reusable presentational components.
`src/containers` | All the Views of app. Every folder contains **1 Presentational Component** of the same name as the folder.**e.g.** `src/containers/Login` has the presentational Component `src/containers/Login/Login.js`. Similarly, it has **1 Container Component** of the name `src/containers/Login/LoginContainer.js`. It may also contain a **CSS file** specific to the Presentational Component `src/containers/Login/Login.scss`.
`src/api` | All the api's to be used in the app.
`src/images` | All the image assets to be used in the app.
`src/routes` | All the routes used in the app.
`src/styles` | All the global styles used in app. Please make the modifications in `src/containers/styles.scss` **only** as during build, `src/containers/styles.css` will be **replaced by the *css* compiled from *scss* file**.
`src/semantic-ui` | Contains [Semantic-UI](https://semantic-ui.com/) theme files. In this folder, mfine specific themes that override Semantic-UI themes are present in folder `src/semantic-ui/src/themes/mfine`. Everything inside this folder can be modified as per design guidelines of mfine and it will be reflected in CA Web App. **Note**: Post updating any file in this folder, you will need to compile semantic-ui. To do this, go to the folder `src/semantic-ui` and run the command `gulp build` and then restart the server.

**P.S. I will keep updating the readme as I add new things. A humble request to all of you to do the same.**

**Thank you.**

**Keep Calm and Code On.** :smile:

